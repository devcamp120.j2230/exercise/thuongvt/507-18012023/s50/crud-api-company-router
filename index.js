// Import thư viện express.js vào. Dạng Import express from "express";
const express = require("express");
const CRUDrouter = require("./app/router/router")



// Khởi tạo app express
const app = new express();

//sử dụng được body json
app.use(express.json());

//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

// Khai báo sẵn 1 port trên hệ thống
const port = 8000;


// sử dụng router
app.use(CRUDrouter);


app.listen(port, () => {
    console.log("App listening on port: ", port);
});
