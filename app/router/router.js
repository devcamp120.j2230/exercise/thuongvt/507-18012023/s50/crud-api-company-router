//Khai báo một thư viện express
const express = require ('express');

const CRUDMiddleware = require("../middleware/middleware");

const companyList = require("../data/data")

//tạo router
const CRUDrouter = express.Router();

// sử dụng router
CRUDrouter.use(CRUDMiddleware);

//lấy tất cả Company
CRUDrouter.get("/companys",(req,res)=>{
    //console.log("get all companys ");
    //console.log(companyList)
    res.status(200).json({
        copany: companyList
    })
});

//lấy 1 Company
CRUDrouter.get("/companys/:id",(req,res)=>{
    let id = req.params.id;

    res.status(200).json({
        message: ` get a companys ${id}`
    })
});


//sửa 1 Company
CRUDrouter.post("/companys",(req,res)=>{
    let body = req.body
    console.log(body);
    res.status(200).json({
       ...body
    })
});

//update 1 Company
CRUDrouter.put("/companys/:id",(req,res)=>{
    let id = req.params.id;
    let body = req.body

    console.log({id, ...body});
    res.status(200).json({
        message: ({id,...body})
    })
});


//xoá 1 Company
CRUDrouter.delete("/companys/:id",(req,res)=>{
    let id = req.params.id;
    console.log(id);
    res.status(200).json({
        message: `delete a company ${id}`
    })
});

module.exports = CRUDrouter;
