//khai báo một class company
class company {
    constructor(id,company,contact,country){
        this.id =id;
        this.company=company;
        this.contact=contact;
        this.country=country;
    }
}

let companyList =[];

// khởi tạo các company
let CP1 = new company(1,"Alfreds Futterkiste","Maria Anders","Germany");
companyList.push(CP1);
let CP2 = new company(2,"Centro comercial Moctezuma","Francisco Chang","Mexico");
companyList.push(CP2);
let CP3 = new company(3,"Ernst Handel","Roland Mendel","Austria");
companyList.push(CP3);
let CP4 = new company(4,"Island Trading","Helen Bennett","UK");
companyList.push(CP4);
let CP5 = new company(5,"Laughing Bacchus Winecellars","Yoshi Tannamuri","Canada");
companyList.push(CP5);
let CP6 = new company(6,"Magazzini Alimentari Riuniti","Giovanni Rovelli","Italy");
companyList.push(CP6);

module.exports={companyList};
